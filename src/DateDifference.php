<?php

namespace Drupal\when_event;

/**
 * Calculates the difference between two dates
 * and determines if an event is happening today,
 * has happened, or will happen in the future.
 */
class DateDifference
{
    // The difference in days of the last calculated
    // date difference
    private $lastDayDifference = -1;

    // Event is happening today
    const DD_HAPPENING = 1;

    // Event is happening in the future
    const DD_FUTURE    = 2;

    // Event is happening tomorrow
    const DD_TOMORROW = 3;

    // Event has already happened
    const DD_PAST      = 4;

    /**
     * Calculates when an event is happening
     * @param string $datetime DateTime formatted string
     * @return int
     */
    public function calculate ($datetime)
    {
        $now  = new \DateTime();
        $date = new \DateTime($datetime);

        $diff = $now->diff($date);
        $this->lastDayDifference = $numDays = (int)$now->diff($date)->format('%R%a');

        if ($now->format('Y-m-d') == $date->format('Y-m-d'))
        {
            return self::DD_HAPPENING;
        }
        else
        {
            if ($diff->invert)
            {
                return self::DD_PAST;
            }
            else
            {
                if ($diff->days == 0)
                {
                    return self::DD_TOMORROW;
                }
                else
                {
                    return self::DD_FUTURE;
                }
            }
        }
    }

    /**
     * Gets the last calculated day difference
     * @param bool $abs if true, returns positive number, else a
     * negative number will be returned for dates in the past
     * @return int
     */
     public function getDifferenceDays ($abs = false)
     {
         return ($abs ? abs($this->lastDayDifference) : $this->lastDayDifference);
     }
}
