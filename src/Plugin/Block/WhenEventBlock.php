<?php

namespace Drupal\when_event\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\when_event\DateDifference;

/**
 * Provides a WhenEvent block.
 *
 * @Block(
 *   id = "when_event",
 *   admin_label = @Translation("WhenEvent block")
 * )
 */
class WhenEventBlock extends BlockBase implements ContainerFactoryPluginInterface {
    protected $dateDifference;

    public function __construct(array $config, $plugin_id, $plugin_definition, DateDifference $dateDiff) {
        parent::__construct($config, $plugin_id, $plugin_definition);
        $this->dateDifference = $dateDiff;

    }

    public static function create(ContainerInterface $container, array $config, $plugin_id, $plugin_definition) {
        return new static (
            $config,
            $plugin_id,
            $plugin_definition,
            $container->get('when_event.date_difference')
        );
    }

    public function build() {
        $node = \Drupal::routeMatch()->getParameter('node');
        $output = null;

        if ($node instanceof \Drupal\node\NodeInterface &&
            $node->getType() == 'event')
        {
            $date = $node->get('field_event_date')->date;
            // In database, date is stored in UTC, use formatter to correct it
            // to users timezone
            $formatted = \Drupal::service('date.formatter')->format(
                $date->getTimestamp(), 'custom', 'Y-m-d H:i:s'
            );

            $difference = $this->dateDifference->calculate($formatted);

            if ($difference == DateDifference::DD_HAPPENING)
            {
                $output = 'This event is happening today';
            }
            else if ($difference == DateDifference::DD_TOMORROW)
            {
                $output = 'This event is happening tomorrow';
            }
            else if ($difference == DateDifference::DD_FUTURE)
            {
                $output = sprintf (
                    '%d days until event starts',
                    $this->dateDifference->getDifferenceDays()
                );
            }
            else if ($difference == DateDifference::DD_PAST)
            {
                $output = sprintf('This event already passed');
            }
        }

        return $output ? ['#markup' => $output] : [];
    }


    public function getCacheMaxAge ()
    {
        return 0;
    }

}
